package com.vicykie.cloud.user.api;

import org.springframework.web.bind.annotation.*;

import java.util.Map;
@RequestMapping("/users")
public interface UserService {


    @GetMapping("")
    Object getUsers();


    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    Object getById(@PathVariable("id") Integer id);

    /**
     * 仅做演示
     * @param user
     * @return
     */
    @PostMapping("")
    Object addUser(@RequestBody Map<String,Object> user);
}
