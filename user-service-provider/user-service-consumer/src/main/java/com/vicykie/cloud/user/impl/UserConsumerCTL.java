package com.vicykie.cloud.user.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.vicykie.cloud.user.IFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserConsumerCTL {

    @Autowired
    private IFeignClient feignClient;
    @HystrixCommand(fallbackMethod = "")
    @GetMapping("/users")
    public Object getUsers1() {
        return feignClient.getUsers();
    }


    @GetMapping("/te/{id}")
    public Object getUsers(@PathVariable("id") int id) {
        return feignClient.getById(id);
    }
}
