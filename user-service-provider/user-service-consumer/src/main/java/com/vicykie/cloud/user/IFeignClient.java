package com.vicykie.cloud.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@FeignClient(value = "${service.user.application-name}", fallback = UserServiceFallBack.class)
public interface IFeignClient {
    @GetMapping("/users")
    Object getUsers();

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    Object getById(@PathVariable("id") Integer id);

}

@Component
class UserServiceFallBack implements IFeignClient {

    @Override
    @GetMapping("/users")
    @ResponseBody
    public Object getUsers() {
        Map<String,Object> map = new HashMap<>();
        map.put("msg","service is unavailable");
        return map;
    }

    @Override
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object getById(@PathVariable("id") Integer id) {
        return "service is unavailable";
    }

}
