package com.vicykie.cloud.user.repository;


import com.vicykie.cloud.user.entity.UserEntity;

public interface UserRepository extends BaseRepository<UserEntity, java.lang.Integer>{

}
