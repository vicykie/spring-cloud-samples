package com.vicykie.cloud.user.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Data
@Entity
@Table(name = "t_user")
public class UserEntity implements Serializable {
    @Id
    private int id;

    private String name;

    private int age;

    private long roleId;


}
