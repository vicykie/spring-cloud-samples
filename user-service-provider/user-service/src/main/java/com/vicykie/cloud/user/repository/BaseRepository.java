package com.vicykie.cloud.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
/**
 * Created by 李朝衡 on 2017/6/15.
 * 基础dao，所有dao接口需继承改类
 */
@NoRepositoryBean
interface BaseRepository<E ,ID extends Serializable> extends CrudRepository<E, ID>, JpaRepository<E, ID>, JpaSpecificationExecutor<E> {
}
