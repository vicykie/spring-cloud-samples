package com.vicykie.cloud.user.impl;

import com.vicykie.cloud.user.api.entity.User;
import com.vicykie.cloud.user.entity.UserEntity;
import com.vicykie.cloud.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class UserServiceImpl {
    @Autowired
    UserRepository userRepository;
    @Value("${server.port}")
    private String port;

    @GetMapping("/users")
    public Object getUsers() {
        System.out.println("---------------------->>>>" + port);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public Object getById(@PathVariable("id") Integer id) {
        return new User(id, "name" + id);
    }


    @PostMapping("/users")
    public Object addUser(@RequestBody Map<String, Object> user) {
        UserEntity entity = new UserEntity();
        entity.setAge(Integer.valueOf(user.get("age").toString()));
        entity.setName(user.get("name").toString());
        entity = userRepository.save(entity);
        return entity;
    }
}
