package com.vicykie.springcloudoauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudOauth2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudOauth2Application.class, args);
    }
}
