package com.vicykie.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringCloudApplication
//@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)

//@EnableResourceServer

public class SpringCloudGatewayV1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudGatewayV1Application.class, args);
    }

}
